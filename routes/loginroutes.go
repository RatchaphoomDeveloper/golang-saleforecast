package routes

import (
	"context"
	"saleforecast/db"
	"saleforecast/middlewares"
	"saleforecast/models"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
)

func Loginroutes(c *fiber.Ctx) error {
	login := models.Login{}
	result := models.Users{}
	err := c.BodyParser(&login)
	if err != nil {
		c.SendStatus(fiber.StatusBadRequest)
	}
	client := db.DbConnections().Collection("Users")
	err = client.FindOne(context.TODO(),
		bson.D{
			{"roleid", login.RoleId},
			{"username", login.Username},
			{"password", login.Password},
		}).Decode(&result)

	
	token, err := middlewares.CreateToken()

	if err != nil {
		c.SendStatus(fiber.StatusBadRequest)
	}
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"code":    fiber.StatusOK,
		"message": "success",
		"token":   token.AccessToken,
		"data":    result,
	})
}
