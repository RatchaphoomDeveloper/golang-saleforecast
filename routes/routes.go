package routes

import (
	"saleforecast/middlewares"

	"github.com/gofiber/fiber/v2"
)

var path = "/api"

func Setup(app *fiber.App) {
	// master := api
	api := app.Group(path)
	api.Post("/login", Loginroutes)
	api.Post("/register", Registerroutes)
	// projects
	api.Use(middlewares.AuthorizationRequired())
	// bearer "jdkffjslkjlkfjlksjdlfjskldjfsjdlkfjlsdjf"
	api.Post("/add_project", AddProjectroutes)
	api.Post("/update_project", UpdateProjectroutes)
	api.Post("/delete_project", DeleteProjectroutes)
	api.Get("/gets_project", GetsProjectroutes)
	api.Get("/get_project", GetProjectroutes)
	// users
	api.Post("/update_user", UpdateUserroutes)
	api.Post("/delete_user", DeleteProjectroutes)
	api.Get("/gets_user", GetsUserroutes)
	api.Get("/get_user", GetUserroutes)
}
