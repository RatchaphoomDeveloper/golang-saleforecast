package routes

import (
	"context"
	"log"
	"saleforecast/db"
	"saleforecast/models"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
)

func AddProjectroutes(c *fiber.Ctx) error {
	project := models.Projects{}
	err := c.BodyParser(&project)
	if err != nil {
		c.SendStatus(fiber.StatusBadRequest)
	}
	projectData := models.Projects{
		Id:             uuid.New().String(),
		ProjectCode:    project.ProjectCode,
		ProjectName:    project.ProjectName,
		ProjectManager: project.ProjectManager,
		Programers:     project.Programers,
		StartDate:      project.StartDate,
		EndDate:        project.EndDate,
		BudGet:         project.BudGet,
		IsActive:       project.IsActive,
	}
	client := db.DbConnections().Collection("Projects")
	insertProject, err := client.InsertOne(context.TODO(), projectData)
	if err != nil {
		c.SendStatus(fiber.StatusBadRequest)
	}
	return c.JSON(fiber.Map{
		"data":    project,
		"message": insertProject.InsertedID,
	})
}

func UpdateProjectroutes(c *fiber.Ctx) error {
	project := models.Projects{}
	err := c.BodyParser(&project)
	if err != nil {
		c.SendStatus(fiber.StatusBadRequest)
	}
	projectData := models.Projects{
		Id:             project.Id,
		ProjectCode:    project.ProjectCode,
		ProjectName:    project.ProjectName,
		ProjectManager: project.ProjectManager,
		Programers:     project.Programers,
		StartDate:      project.StartDate,
		EndDate:        project.EndDate,
		BudGet:         project.BudGet,
		IsActive:       project.IsActive,
	}
	client := db.DbConnections().Collection("Projects")
	updateProjects, err := client.UpdateByID(context.TODO(), bson.D{{"_id", projectData.Id}}, projectData)
	if err != nil {
		c.SendStatus(fiber.StatusBadRequest)
	}
	return c.JSON(fiber.Map{
		"data": updateProjects,
	})
}

func DeleteProjectroutes(c *fiber.Ctx) error {
	project := models.Projects{}
	err := c.BodyParser(&project)
	if err != nil {
		c.SendStatus(fiber.StatusBadRequest)
	}
	client := db.DbConnections().Collection("Projects")
	deleteProject, err := client.DeleteOne(context.TODO(), bson.D{{"_id", project.Id}})
	if err != nil {
		c.SendStatus(fiber.StatusBadRequest)
	}
	return c.JSON(fiber.Map{
		"data": deleteProject,
	})
}

func GetsProjectroutes(c *fiber.Ctx) error {
	var project []models.Projects
	client := db.DbConnections().Collection("Projects")
	cursor, err := client.Find(context.TODO(), bson.D{{}})
	if err != nil {
		c.SendStatus(fiber.StatusBadRequest)
	}
	if err = cursor.All(context.TODO(), &project); err != nil {
		log.Fatal(err)
	}
	return c.JSON(fiber.Map{
		"data": project,
	})
}

func GetProjectroutes(c *fiber.Ctx) error {
	result := models.Projects{}
	client := db.DbConnections().Collection("Projects")
	// dflksldkf;ksd;kf;sdk;fk;sdkfsdfl;sdkf;ksdfk;sdkf
	// dkfsl;kfksdkf;skd;lfksl;dkf;ksd;fk;sdkfksd;f;sdkf
	err := client.FindOne(context.TODO(),
		bson.D{
			{"_id", c.Query("id")},
		}).Decode(&result)
	if err != nil {
		c.SendStatus(fiber.StatusBadRequest)
	}
	return c.JSON(fiber.Map{
		"data": result,
	})
}
