package routes

import (
	"context"
	"saleforecast/db"
	"saleforecast/models"

	"github.com/gofiber/fiber/v2"
)

func Registerroutes(c *fiber.Ctx) error {
	register := models.Users{}
	err := c.BodyParser(&register)
	if err != nil {
		c.SendStatus(fiber.StatusBadRequest)
	}
	client := db.DbConnection()
	registerResult, err := client.Collection("Users").InsertOne(context.TODO(), register)
	if err != nil {
		c.SendStatus(fiber.StatusBadRequest)
	}
	return c.Status(fiber.StatusCreated).JSON(fiber.Map{
		"code":    fiber.StatusCreated,
		"message": "register is success",
		"data": map[string]interface{}{
			"id": registerResult.InsertedID,
		},
	})
}
