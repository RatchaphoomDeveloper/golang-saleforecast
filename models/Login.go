package models

type Login struct {
	RoleId   int    `json:"RoleId"`
	Username string `json:"Username"`
	Password string `json:"Password"`
}


type Token struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

