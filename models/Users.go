package models

type Users struct {
	Id       string `json:"Id" bson:"_id"`
	RoleId   int    `json:"RoleId"`
	Username string `json:"Username"`
	Password string `json:"Password"`
	Name     string `json:"Name"`
	Lastname string `json:"Lastname"`
	IsActive bool   `json:"IsActive"`
}
