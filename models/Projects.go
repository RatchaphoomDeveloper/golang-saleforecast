package models

type Projects struct {
	Id             string  `json:"Id" bson:"_id"`
	ProjectCode    string  `json:"ProjectCode" bson:"projectCode"`
	ProjectName    string  `json:"ProjectName" bson:"projectName"`
	ProjectManager string  `json:"ProjectManager" bson:"projectmanager"`
	Programers     []Users `json:"Programmers" bson:"programmers"`
	StartDate      string  `json:"StartDate" bson:"startdate"`
	EndDate        string  `json:"EndDate" bson:"enddate"`
	BudGet         float64 `json:"BudGet" bson:"budget"`
	IsActive       bool    `json:"IsActive" bson:"isactive"`
}
