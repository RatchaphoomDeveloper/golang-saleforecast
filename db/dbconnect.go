package db

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func DbConnection() *mongo.Database {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://attraadmin:attra187540@139.59.223.184:27017/GoTraining?authSource=admin"))
	if err != nil {
		log.Fatal(err)
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Mongodb is connecting .... !")
	return client.Database("GoTraining")
}

func DbConnections() *mongo.Database {
	clientOptions := options.Client().ApplyURI("mongodb://attraadmin:attra187540@139.59.223.184:27017/GoTraining?authSource=admin")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		fmt.Println(err)
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Mongodb is connecting .... !")
	return client.Database("GoTraining")
}
