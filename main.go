package main

import (
	"fmt"
	"saleforecast/routes"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func main() {
	app := fiber.New(fiber.Config{
		BodyLimit: 512 * 1024 * 1024,
	})
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowMethods: "GET,POST,PUT",
		AllowHeaders: "Content-Type, Authorization",
	}))

	routes.Setup(app)

	app.Listen(fmt.Sprintf(":%s", "8080"))
}
